using System;
using Poker.Code.Data.Enums;
using Poker.Code.Data.StaticData;
using Poker.Code.Services.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace Poker.Code.Core.UI.Gameplay.PlayerChoice
{
    public class PlayerChoiceButtons : MonoBehaviour
    {
        public event Action OnRaiseClick;
        public event Action OnCheckCallClick;
        public event Action OnFoldClick;
        public event Action OnAllInClick;

        [SerializeField] private AnimatedInteractButton _raiseButton;
        [SerializeField] private AnimatedInteractButton _checkCallButton;
        [SerializeField] private AnimatedInteractButton _foldButton;
        [SerializeField] private AnimatedInteractButton _allInButton;

        private UserInputSprites _userInputSprites;
        private ISoundService _soundService;

        private void Awake()
        {
            _raiseButton.onClick.AddListener(OnRaiseButtonClick);
            _checkCallButton.onClick.AddListener(OnCheckCallButtonClick);
            _foldButton.onClick.AddListener(OnFoldButtonClick);
            _allInButton.onClick.AddListener(OnAllInButtonClick);
        }

        private void OnDestroy()
        {
            _raiseButton.onClick.RemoveAllListeners();
            _checkCallButton.onClick.RemoveAllListeners();
            _foldButton.onClick.RemoveAllListeners();
            _allInButton.onClick.RemoveAllListeners();
        }

        public void Construct(UserInputSprites userInputSprites, ISoundService soundService)
        {
            _soundService = soundService;
            _userInputSprites = userInputSprites;
        }

        public void SetButtonsActive(bool raise, bool checkCall, bool fold, bool allIn)
        {
            _raiseButton.SetInteractable(raise);
            _checkCallButton.SetInteractable(checkCall);
            _foldButton.SetInteractable(fold);
            _allInButton.SetInteractable(allIn);
        }

        public void SetCallView()
        {
            _checkCallButton.image.sprite = _userInputSprites.CallSprite;
            _checkCallButton.spriteState = SetPressedSprite(_userInputSprites.CallPressedSprite);;
        }

        public void SetCheckView()
        {
            _checkCallButton.image.sprite = _userInputSprites.CheckSprite;
            _checkCallButton.spriteState = SetPressedSprite(_userInputSprites.CheckPressedSprite);
        }

        private SpriteState SetPressedSprite(Sprite pressedSprite)
        {
            SpriteState spriteState = _checkCallButton.spriteState;
            spriteState.pressedSprite = pressedSprite;
            return spriteState;
        }

        private void OnRaiseButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnRaiseClick?.Invoke();
        }

        private void OnCheckCallButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnCheckCallClick?.Invoke();
        }

        private void OnFoldButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnFoldClick?.Invoke();
        }

        private void OnAllInButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnAllInClick?.Invoke();
        }
    }
}