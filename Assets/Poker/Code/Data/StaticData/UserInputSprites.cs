using UnityEngine;

namespace Poker.Code.Data.StaticData
{
    [CreateAssetMenu(fileName = "User Input Sprites", menuName = "Static Data/User Input Sprites")]
    public class UserInputSprites : ScriptableObject
    {
        public Sprite CheckSprite;
        public Sprite CheckPressedSprite;
        public Sprite CallSprite;
        public Sprite CallPressedSprite;
    }
}