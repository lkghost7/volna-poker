using System;

namespace Poker.Code.Data.Progress
{
    [Serializable]
    public class PlayerProgress
    {
        public Balance Balance;
        public Settings Settings;
        public int CurrentDay;

        public PlayerProgress(int startCoins, int day)
        {
            Balance = new Balance(startCoins);
            Settings = new Settings();
            CurrentDay = day;
        }
    }
}