using ChestsGame.CodeBase.Data.StaticData;
using Poker.Code.Data.StaticData;
using Poker.Code.Data.StaticData.Locations;
using Poker.Code.Data.StaticData.Sounds;
using Poker.Code.Infrastructure.ServiceContainer;

namespace Poker.Code.Services.StaticData
{
    public interface IStaticData : IService
    {
        GameConfig GameConfig { get; }
        LocationData LocationData { get; }
        UserInputSprites UserInputSprites { get; }
        SoundData SoundData { get; }
        ChestsConfig ChestsConfig { get; }
        ChestsPrefabs ChestsPrefabs { get; }
        void LoadStaticData();
    }
}