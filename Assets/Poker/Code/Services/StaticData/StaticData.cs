using ChestsGame.CodeBase.Data.StaticData;
using Poker.Code.Data.StaticData;
using Poker.Code.Data.StaticData.Locations;
using Poker.Code.Data.StaticData.Sounds;
using Poker.Code.Services.Providers.StaticDataProvider;

namespace Poker.Code.Services.StaticData
{
    public class StaticData : IStaticData
    {
        public GameConfig GameConfig { get; private set; }
        public LocationData LocationData { get; private set; }
        public UserInputSprites UserInputSprites { get; private set; }
        public SoundData SoundData { get; private set; }
        
        public ChestsConfig ChestsConfig { get; private set; }
        public ChestsPrefabs ChestsPrefabs { get; private set; }

        private readonly IStaticDataProvider _staticDataProvider;

        public StaticData(IStaticDataProvider staticDataProvider) =>
            _staticDataProvider = staticDataProvider;

        public void LoadStaticData()
        {
            GameConfig = _staticDataProvider.GetGameConfig();
            LocationData = _staticDataProvider.GetLocationData();
            UserInputSprites = _staticDataProvider.GetUserInputSprites();
            SoundData = _staticDataProvider.GetSoundData();

            ChestsConfig = _staticDataProvider.GetChestsConfigData();
            ChestsPrefabs = _staticDataProvider.GetChestsPrefabsData();
        }
    }
}