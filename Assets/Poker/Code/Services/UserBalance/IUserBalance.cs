using Poker.Code.Core.Players;
using Poker.Code.Core.UI.Gameplay.TopPanel;
using Poker.Code.Infrastructure.ServiceContainer;

namespace Poker.Code.Services.UserBalance
{
    public interface IUserBalance : IService
    {
        void Construct(Player user, UserInfoTexts infoTexts);
        void SaveBalance();
        void ResetUserBalance();
        void ClearGameplayDependencies();
        void AddBonusForBalance(int bonus);
    }
}