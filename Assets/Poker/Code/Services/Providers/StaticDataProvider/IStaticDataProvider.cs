using ChestsGame.CodeBase.Data.StaticData;
using Poker.Code.Data.StaticData;
using Poker.Code.Data.StaticData.Locations;
using Poker.Code.Data.StaticData.Sounds;
using Poker.Code.Infrastructure.ServiceContainer;

namespace Poker.Code.Services.Providers.StaticDataProvider
{
    public interface IStaticDataProvider : IService
    {
        GameConfig GetGameConfig();
        LocationData GetLocationData();
        UserInputSprites GetUserInputSprites();
        SoundData GetSoundData();
        ChestsConfig GetChestsConfigData();
        ChestsPrefabs GetChestsPrefabsData();
    }
}