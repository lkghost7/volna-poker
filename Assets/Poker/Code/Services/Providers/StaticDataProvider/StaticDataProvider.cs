using ChestsGame.CodeBase.Data.StaticData;
using Poker.Code.Data.StaticData;
using Poker.Code.Data.StaticData.Locations;
using Poker.Code.Data.StaticData.Sounds;
using UnityEngine;

namespace Poker.Code.Services.Providers.StaticDataProvider
{
    public class StaticDataProvider : IStaticDataProvider
    { 
        private const string GameConfigPath = "StaticData/Game Config";
        private const string LocationDataPath = "StaticData/Location Data";
        private const string UserInputSpritesPath = "StaticData/User Input Sprites";
        private const string SoundDataPath = "StaticData/Sound Data";
        
        private const string ChestsConfigPath = "StaticData/ChestsConfig";
        private const string ChestsPrefabsPath = "StaticData/ChestsPrefabs";

        public GameConfig GetGameConfig() => Resources.Load<GameConfig>(GameConfigPath);

        public LocationData GetLocationData() => Resources.Load<LocationData>(LocationDataPath);

        public UserInputSprites GetUserInputSprites() => Resources.Load<UserInputSprites>(UserInputSpritesPath);

        public SoundData GetSoundData() => Resources.Load<SoundData>(SoundDataPath);
        
        public ChestsConfig GetChestsConfigData() => Resources.Load<ChestsConfig>(ChestsConfigPath);
        public ChestsPrefabs GetChestsPrefabsData() => Resources.Load<ChestsPrefabs>(ChestsPrefabsPath);
    }
}