using ChestsGame.CodeBase.Core.UI;
using ChestsGame.CodeBase.Infrastructure.Factory;
using Poker.Code.Services.Factories.UIFactory;
using Poker.Code.Services.SceneLoader;
using Poker.Code.Services.StaticData;
using Poker.Code.Services.UserBalance;

namespace Poker.Code.Infrastructure.StateMachine.States
{
    public class ChestsState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ISceneLoader _sceneLoader;
        private readonly IUIFactory _uiFactory;
        private readonly IUserBalance _userBalance;
        private readonly IChestsFactory _factory;
        private bool _isPayloaded;
        private float _chestValue;
        private float _winAmount;
        private float _bet;
        
        private const string ChestsSceneName = "Chests";

        public ChestsState(IGameStateMachine stateMachine, ISceneLoader sceneLoader, 
            IUIFactory uiFactory, IUserBalance userBalance, IStaticData staticData)
        {
            _uiFactory = uiFactory;
            _userBalance = userBalance;
            _sceneLoader = sceneLoader;
            _stateMachine = stateMachine;
            _factory = new ChestsFactory(staticData.ChestsPrefabs, staticData.ChestsConfig);
        }
        
        public void Enter()
        {
            _sceneLoader.LoadScene(ChestsSceneName, CompleteLoad);
        }

        private void CompleteLoad() => LoadChestsGame();

        private void LoadChestsGame(float payload = 0)
        {
            if (payload > 0)
            {
                _isPayloaded = true;
                _bet = payload;
            }
            CreateGame();
        }
        
        public void Exit()
        {
            UnSubscribe();
        }

        private void CreateGame()
        {
            _factory.CreateUIRoot();
            _factory.CreateChests(_isPayloaded);
            _factory.CreateExitButton();
            _factory.ExitButton.onClick.AddListener(ExitMainMenu);
            _factory.CreateWinPopUp();
        
            SubscribeChests();
            SubscribeWinPopUp();
        }

        private void ExitMainMenu() => _stateMachine.Enter<MainMenuState>();

        public void OnPayout()
        {
            _winAmount = CalculateWin(_chestValue);
            _userBalance.AddBonusForBalance((int)_winAmount);
            ShowWinPopUp(_winAmount);
            //save
        }

        private void SubscribeWinPopUp() => _factory.WinPopUp.OnPopUpClick += ExitGame;

        private void ExitGame()
        {
            _stateMachine.Enter<MainMenuState>();
        }

        private void SubscribeChests()
        {
            foreach (Chest chest in _factory.Chests)
            {
                chest.View.OnChestClicked += GameLoop;
                chest.OnChestOpen += GetChestValue;
            }
        }
        private void GetChestValue(float value) => _chestValue = value;
        private void GameLoop()
        {
            DisableChests();
            OnPayout();
        }

        private void DisableChests()
        {
            foreach (Chest chest in _factory.Chests) 
                chest.View.DisableButton();
        }

        private void ShowWinPopUp(float winAmount) => _factory.WinPopUp.Show(winAmount);

        private float CalculateWin(float winChestValue)
        {
            if (!_isPayloaded) 
                return winChestValue;

            return winChestValue * _bet;
        }

        private void UnSubscribe()
        {
            _factory.ExitButton.onClick.RemoveListener(ExitGame);
            _factory.WinPopUp.OnPopUpClick -= ExitGame;
            foreach (Chest chest in _factory.Chests)
                chest.View.OnChestClicked -= GameLoop;
        }
    }
}

