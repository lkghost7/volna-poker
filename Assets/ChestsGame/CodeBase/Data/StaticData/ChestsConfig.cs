using UnityEngine;

namespace ChestsGame.CodeBase.Data.StaticData
{
    [CreateAssetMenu(fileName = "ChestsConfig", menuName = "StaticData/ChestsConfig")]
    public class ChestsConfig : ScriptableObject
    {
        public float[] Wins;
    }
}