using UnityEngine;

namespace ChestsGame.CodeBase.Data.StaticData
{
    [CreateAssetMenu(fileName = "ChestsPrefabs", menuName = "StaticData/ChestsPrefabs")]
    public class ChestsPrefabs : ScriptableObject
    {
        public GameObject UIRoot;
        public GameObject ChestRoot;
        public GameObject Chest;
        public GameObject WinPopUp;
        public GameObject ExitButton;
    }
}