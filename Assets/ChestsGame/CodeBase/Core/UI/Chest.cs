using System;

namespace ChestsGame.CodeBase.Core.UI
{
    public class Chest
    {
        public event Action<float> OnChestOpen;
        
        public ChestView View;
        public float Value { get; private set; }

        public Chest(ChestView view, float value)
        {
            View = view;
            Value = value;
            SetValue(value);
            View.OnChestClicked += ChestOpen;
        }

        private void ChestOpen() => OnChestOpen?.Invoke(Value);
        
        private void SetValue(float value) => View.SetChestValueText(value);
    }
}