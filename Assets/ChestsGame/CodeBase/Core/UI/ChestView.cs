using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ChestsGame.CodeBase.Core.UI
{
    public class ChestView : MonoBehaviour
    {
        public event Action OnChestClicked;

        [SerializeField] private Button _openButton;
        [SerializeField] private Image _chestImage;
        [SerializeField] private GameObject _winImage;
        [SerializeField] private TMP_Text _value;

        private void Start() => _openButton.onClick.AddListener(OnClick);

        public void SetChestValueText(float value) => 
            _value.text = value.ToString();

        public void DisableButton() => _openButton.interactable = false;

        public void ShowWinImage()
        {
            _chestImage.enabled = false;
            _winImage.SetActive(true);
        }

        private void OnClick()
        {
            ShowWinImage();
            OnChestClicked?.Invoke();
        }

        private void OnDestroy() => _openButton.onClick.RemoveListener(OnClick);
    }
}