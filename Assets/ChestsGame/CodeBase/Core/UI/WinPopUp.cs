using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ChestsGame.CodeBase.Core.UI
{
    public class WinPopUp : MonoBehaviour
    {
        public event Action OnPopUpClick;
        
        [SerializeField] private GameObject _winPopUp;
        [SerializeField] private TMP_Text _winText;
        [SerializeField] private Button _button;
        [SerializeField] private CanvasGroup _canvasGroup;

        private void Start()
        {
            _button.onClick.AddListener(OnButtonClicked);
            _canvasGroup = _winPopUp.GetComponent<CanvasGroup>();
            _winPopUp.SetActive(false);
        }

        public void Show(float winAmount)
        {
            SetWinText(winAmount);
            _winPopUp.SetActive(true);
            _canvasGroup.alpha = 0;
            _canvasGroup.GetComponent<CanvasGroup>().DOFade(1, 1).SetDelay(0.5f);
        }

        private void OnButtonClicked()
        {
            OnPopUpClick?.Invoke();
        }

        private void SetWinText(float winAmount) => _winText.text = $"You win {winAmount}";
        private void OnDestroy() => _button.onClick.RemoveListener(OnButtonClicked);
    }
}