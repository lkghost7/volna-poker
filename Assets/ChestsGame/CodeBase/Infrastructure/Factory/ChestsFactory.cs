using ChestsGame.CodeBase.Core.UI;
using ChestsGame.CodeBase.Data.Extensions;
using ChestsGame.CodeBase.Data.StaticData;
using UnityEngine;
using Button = UnityEngine.UI.Button;
using Object = UnityEngine.Object;

namespace ChestsGame.CodeBase.Infrastructure.Factory
{
    public class ChestsFactory : IChestsFactory
    {
        private readonly ChestsPrefabs _prefabs;
        private readonly ChestsConfig _config;
        private Transform _uiRoot;

        public Chest[] Chests { get; private set; }
        public WinPopUp WinPopUp { get; private set; }
        public Button ExitButton { get; private set; }

        public ChestsFactory(ChestsPrefabs prefabs, ChestsConfig config)
        {
            _prefabs = prefabs;
            _config = config;
            Chests = new Chest[_config.Wins.Length];
        }

        public void CreateChests(bool isPayloaded)
        {
            Transform chestRoot = CreateUIElement(_prefabs.ChestRoot, _uiRoot).transform;

            GameplayExtensions.Shuffle<float>(_config.Wins);
            
            for(int i = 0; i< _config.Wins.Length; i++)
                Chests[i] = CreateChest(chestRoot, isPayloaded, i);
        }

        private Chest CreateChest(Transform chestRoot, bool isPayloaded, int i)
        {
            ChestView chestView = CreateUIElement(_prefabs.Chest, chestRoot).GetComponent<ChestView>();

            Chest chest = new Chest(chestView, _config.Wins[i]);
            return chest;
        }

        public Button CreateExitButton() => ExitButton = CreateUIElement(_prefabs.ExitButton, _uiRoot).GetComponent<Button>();

        public void CreateWinPopUp() => 
            WinPopUp = CreateUIElement(_prefabs.WinPopUp, _uiRoot).GetComponent<WinPopUp>();


        public void CreateUIRoot() =>
            _uiRoot = Object.Instantiate(_prefabs.UIRoot).transform;

        private GameObject CreateUIElement(GameObject prefab, Transform root) => 
            Object.Instantiate(prefab, root);
    }
}