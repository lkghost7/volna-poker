using ChestsGame.CodeBase.Core.UI;
using UnityEngine.UI;

namespace ChestsGame.CodeBase.Infrastructure.Factory
{
    public interface IChestsFactory
    {
        void CreateUIRoot();
        void CreateChests(bool isPayloaded);
        Chest[] Chests { get; }
        WinPopUp WinPopUp { get; }
        Button ExitButton { get; }
        void CreateWinPopUp();
        Button CreateExitButton();
    }
}