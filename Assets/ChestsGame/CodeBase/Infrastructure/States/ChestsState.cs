using ChestsGame.CodeBase.Core.UI;
using ChestsGame.CodeBase.Infrastructure.Factory;

namespace ChestsGame.CodeBase.Infrastructure.States
{
    public class ChestsState
    {
        private readonly IChestsFactory _factory;
        private bool _isPayloaded;
        private float _chestValue;
        private float _winAmount;
        private float _bet;

        public ChestsState(IChestsFactory factory)
        {
            _factory = factory;
        }
        public void Enter(float payload = 0)
        {
            if (payload > 0)
            {
                _isPayloaded = true;
                _bet = payload;
            }
            CreateGame();
        }

        public void Exit()
        {
            UnSubscribe();
        }

        private void CreateGame()
        {
            _factory.CreateUIRoot();
            _factory.CreateChests(_isPayloaded);
            _factory.CreateExitButton();
            _factory.ExitButton.onClick.AddListener(ExitGame);
            _factory.CreateWinPopUp();
        
            SubscribeChests();
            SubscribeWinPopUp();
        }

        public void OnPayout()
        {
            _winAmount = CalculateWin(_chestValue);
            ShowWinPopUp(_winAmount);
            //save
            ExitGame();
        }

        private void SubscribeWinPopUp() => _factory.WinPopUp.OnPopUpClick += ExitGame;

        private void ExitGame()
        {
            //state machine logic
        }

        private void SubscribeChests()
        {
            foreach (Chest chest in _factory.Chests)
            {
                chest.View.OnChestClicked += GameLoop;
                chest.OnChestOpen += GetChestValue;
            }
        }
        private void GetChestValue(float value) => _chestValue = value;
        private void GameLoop()
        {
            DisableChests();
            OnPayout();
        }

        private void DisableChests()
        {
            foreach (Chest chest in _factory.Chests) 
                chest.View.DisableButton();
        }

        private void ShowWinPopUp(float winAmount) => _factory.WinPopUp.Show(winAmount);

        private float CalculateWin(float winChestValue)
        {
            if (!_isPayloaded) 
                return winChestValue;

            return winChestValue * _bet;
        }

        private void UnSubscribe()
        {
            _factory.ExitButton.onClick.RemoveListener(ExitGame);
            _factory.WinPopUp.OnPopUpClick -= ExitGame;
            foreach (Chest chest in _factory.Chests)
                chest.View.OnChestClicked -= GameLoop;
        }
    }
}
